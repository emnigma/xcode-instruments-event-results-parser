import re
import unicodedata

def read_events(filename: str="./events.txt") -> list[str]:
    with open(filename, "r") as events_file:
        return events_file.read().splitlines()

def parse_bench_results(input) -> list[str]:
    unicode = unicodedata.normalize("NFKD", input)

    no_slash_tees = re.sub("\t", "", unicode)
    no_spaces = re.sub(" ", "", no_slash_tees)
    no_first_entry_seconds = re.sub("\d*\.\d*s", "", no_spaces)

    parsed = re.split(r"\d{2}\.\d%", no_first_entry_seconds)[1:-1]
    return parsed

def export_results(input: str, export_filename: str) -> None:

    events = read_events()
    parsed_results = parse_bench_results(input)
    if len(parsed_results) != len(events):
        raise RuntimeError("Defined and parsed events don't match")

    with open(export_filename, "w+") as out:
        export = []
        for (name, num) in zip(events, parsed_results):
            export.append(f"{name}: {num}\n")
        out.writelines(export)
