from parse_bench import export_results


def main():
    SYNTH_BENCH_ID = "6"
    DEFINE_DIRECTIVE = "FR_ORIG"
    OPTIONAL = ""
    filename = f"./synth_bench_{SYNTH_BENCH_ID}_{DEFINE_DIRECTIVE}{OPTIONAL}.txt"

    # put copied input here:
    input = "4.87 s   99.7%	4.87 s	149 835 903   99.9%	55 399 090   99.9%	81 743 993   95.1%	18 454 413   81.7%	10 086 347   99.9%	28 577 936 562   99.9%	907 351   99.9%	1 619 791   82.9%	 	     HotLoop"
    
    export_results(input=input, export_filename=filename)

if __name__ == "__main__":
    main()