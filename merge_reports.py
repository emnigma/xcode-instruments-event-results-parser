import os
import re

def modify_string(old: str) -> str:
    num = re.search(r"\d+$", old).group(0)
    new_num = ""

    for i, item in enumerate(reversed(num)):
        if i > 0 and i % 3 == 0:
            new_num += "_"
        new_num += item

    new = re.sub(r"\d+$", "", old)+new_num[::-1]

    return new

def merge_txts(dir: str):
    for root, _, files in os.walk(dir):
        bench_results = [os.path.join(root, file) for file in files if file.endswith(".txt")]
        if not bench_results:
            continue

        reports = []
        for file in bench_results:
            with open(file, "r") as file:
                reports.extend(file.read().splitlines())

        reports = [modify_string(item) + '\n' for item in reports]

        merged_filename = f"{os.path.commonprefix(bench_results)}_full.txt"

        with open(merged_filename, "w+") as merged_file:
            merged_file.writelines(reports)

        for item in bench_results:
            os.remove(item)

def main():
    dir = "<your dir>"
    merge_txts(dir)

if __name__ == "__main__":
    main()